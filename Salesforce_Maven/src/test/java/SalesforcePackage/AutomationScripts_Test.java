package SalesforcePackage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.testng.reporters.XMLReporter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class AutomationScripts_Test extends ReUsableMethods
{
	static WebDriver driver;
//	public static void main(String[] args) throws InterruptedException 
//	{

//		LoginErrorMessage_1();
//		LoginToSalesForce_2();
//		CheckRemeberMe_3();
//		ForgotPassword_4A();
//		ForgotPassword_4B();
//		UserMenuDropDown_TC05();
//		UserMenuDropDown_TC06();
//		reports.endTest(loggers);
//		// reports.flush();
//		loggerEnd();
//	}

	public static void launchURL()
	{
		System.setProperty("webdriver.chrome.driver", "C:\\MyProject\\drivers\\chromedriver.exe");
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		driver.manage().window().maximize();
	}
	@AfterClass
	public static void loggerEnd()
	{
		reports.endTest(loggers);
		reports.flush();
		//createTestScriptReports
	}
	public static void userPwdEntry(String uName, String pwd) throws InterruptedException 
	{
		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
		 enterText(userName,uName,"Username");
		 Thread.sleep(2000);
		 
		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		enterText(passWord,pwd,"Password");	
		Thread.sleep(2000);	
	}
	@Test(priority=1)
	public static void LoginErrorMessage_1() throws InterruptedException
	{    
		launchURL();
		createTestScriptReports("Login Error Message - 1");
		
		 WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
		 enterText(userName,"asit.awasthi-c9jk@force.com","Username");

		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
		Thread.sleep(2000);
		clearTextBox(passWord, "Password");
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		clickButton(logIn, "Login Button");
		
		WebElement errorMsg1=driver.findElement(By.xpath("//div[@id='error']"));
		String str="Please enter your password.";
		validateErrorMsg(errorMsg1,str,"Error message" );

	}
	@Test(priority=2)
	public static void LoginToSalesForce_2() throws InterruptedException 
	{
		launchURL();
		createTestScriptReports("Login To SalesForce -2");
	
		userPwdEntry("asit.awasthi-c9jk@force.com","rolly123");
//		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
//		enterText(userName,"asit.awasthi-c9jk@force.com","Username");
//		Thread.sleep(2000);	
//	 
//		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
//		enterText(passWord,"rolly123","Password");	
//		Thread.sleep(2000);		
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		clickButton(logIn, "Login Button");
		Thread.sleep(2000);	
	}
	@Test(priority=3)
	public static void CheckRemeberMe_3() throws InterruptedException
	{
		launchURL();
		createTestScriptReports("Check RemeberMe - 3");
		userPwdEntry("asit.awasthi-c9jk@force.com","rolly123");
//		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
//		enterText(userName,"asit.awasthi-c9jk@force.com","Username");
//		Thread.sleep(2000);	
//	 
//		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
//		enterText(passWord,"rolly123","Password");	
//		Thread.sleep(2000);		
		
		WebElement chkbox=driver.findElement(By.xpath("//input[@id='rememberUn']"));
		checkBoxChecked(chkbox, "Remember me checkbox");
		Thread.sleep(2000);
			
		WebElement logIn3=driver.findElement(By.xpath("//input[@id='Login']"));
		clickButton(logIn3, "Login Button");
		Thread.sleep(2000);

		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		System.out.println("Pass: Popup window is closed");
		
		WebElement userProfile=driver.findElement(By.xpath("//span[@id='userNavLabel']"));
		clickLink(userProfile, "User menu dropdown");
		Thread.sleep(5000);
		
		WebElement logOut=driver.findElement(By.xpath("//a[contains(text(),'Logout')]"));
		clickLink(logOut, "Logout");
		Thread.sleep(5000);
	
		WebElement usernameCheck=driver.findElement(By.xpath("//span[@id='idcard-identity']"));	
		Thread.sleep(5000);
		if(usernameCheck.getText().equals("asit.awasthi-c9jk@force.com"))
			System.out.println("Pass: Username is displayed ");
		else
			System.out.println("Fail: Username is not displayed");
		
		WebElement checkboxCheck=driver.findElement(By.xpath("//input[@id='rememberUn']"));
		if(checkboxCheck.isSelected())
			System.out.println("Pass: Remember me checkbox is checked");
		else
			System.out.println("Fail: Remember me checkbox is not checked");
			
	}
	@Test(priority=4)
	public static void ForgotPassword_4A() throws InterruptedException
	{
		launchURL();
		createTestScriptReports("Forgot Password-4A");
		
		WebElement forgotPwd=driver.findElement(By.xpath("//a[@id='forgot_password_link']"));
		clickLink(forgotPwd, "Forgot password link");
		
		if (driver.getCurrentUrl().equals("https://login.salesforce.com/secur/forgotpassword.jsp?locale=us"))
			System.out.println("Pass: Forgot password page is loaded");
		else
			System.out.println("Fail: Forgot password page is not loaded");
		Thread.sleep(2000);
		
		WebElement ForgotUsername=driver.findElement(By.xpath("//input[@id='un']"));
		enterText(ForgotUsername,"asit.awasthi-c9jk@force.com", "Username");
		
		WebElement continueButton=driver.findElement(By.xpath("//input[@id='continue']"));
		clickButton(continueButton, "Continue button");
		Thread.sleep(2000);
		
		WebElement pwdMsg=driver.findElement(By.xpath("//p[contains(text(),'We�ve sent you an email with a link to finish rese')]"));
		String str="We�ve sent you an email with a link to finish resetting your password.";
		validateErrorMsg(pwdMsg,str, "Message");
		
	}
	@Test(priority=5)
	public static void ForgotPassword_4B() throws InterruptedException
	{
		launchURL();
		createTestScriptReports("Forgot Password-4B");
		userPwdEntry("123","22131");
//		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
//		enterText(userName, "123", "Username");	
//		Thread.sleep(2000);
//		
//		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
//		enterText(passWord, "22131", "Password");	
//		Thread.sleep(2000);
//		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		clickButton(logIn,"Login button");
		Thread.sleep(2000);
		
		WebElement errorMsg=driver.findElement(By.xpath("//div[@id='error']"));
		String str="Please check your username and password. If you still can't log in, contact your Salesforce administrator.";
		validateErrorMsg(errorMsg, str, "Error message");
		Thread.sleep(2000);
	}
	@Test(priority=6, enabled=false)
	public static void UserMenuDropDown_TC05() throws InterruptedException
	{
		launchURL();
		createTestScriptReports("User Menu Drop Down-TC05");

		userPwdEntry("123","22131");
		
//		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
//		enterText(userName, "123", "Username");	
//		Thread.sleep(2000);
//		
//		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
//		enterText(passWord, "22131", "Password");	
//		Thread.sleep(2000);
		
		WebElement logIn=driver.findElement(By.xpath("//input[@id='Login']"));
		clickButton(logIn,"Login button");
		Thread.sleep(2000);
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		//loggers.log(LogStatus.INFO, "Popup window is closed");
				
		WebElement userMenu=driver.findElement(By.xpath("//div[@id='userNavButton']"));
		clickMenu(userMenu, "User menu");
		Thread.sleep(2000);	

		Boolean myProfilefound = false;
		Boolean mySetting = false;
		Boolean devConsole = false;
		Boolean logOut = false;
		
		List<WebElement> dropDownElement = driver.findElements(By.className("mbrMenuItems"));
		for(WebElement value : dropDownElement) 
		{
		   //System.out.println(value.getText());
			if(value.getText().contains("My Profile") )
		   {
		    	myProfilefound=true;
		       // break;
		    }
			if (value.getText().contains("My Settings")) 
				{
				mySetting=true;
				}
			if (value.getText().contains("Developer Console")) 
			{
				devConsole=true;
			}
			if (value.getText().contains("Logout"))
			{
				logOut=true;
			}
		
		}
		
		if(myProfilefound && mySetting && devConsole && logOut) {
		    System.out.println("Value exists");
		    loggers.log(LogStatus.INFO, "Drop Down with User Profile, My Settings, Developer Console and Logout is displayed successfully");
		}
		else
		{
			loggers.log(LogStatus.FAIL, "Drop Down with User Profile, My Settings, Developer Console and Logout is not displayed ");	
		}
	}
	@Test(priority=7, enabled=false)
	public static void UserMenuDropDown_TC06() throws InterruptedException
	{
		launchURL();
		createTestScriptReports("User Menu Drop Down-TC05");
		userPwdEntry("asit.awasthi-c9jk@force.com","rolly123");
//		WebElement userName=driver.findElement(By.xpath("//input[@id='username']"));
//		enterText(userName,"asit.awasthi-c9jk@force.com","Username");
//		Thread.sleep(2000);	
//	 
//		WebElement passWord=driver.findElement(By.xpath("//input[@id='password']"));
//		enterText(passWord,"rolly123","Password");	
//		Thread.sleep(2000);		
		
		WebElement logIn3=driver.findElement(By.xpath("//input[@id='Login']"));
		clickButton(logIn3, "Login Button");
		Thread.sleep(2000);
		
		WebElement closeWindow=driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closeWindow.click();
		Thread.sleep(3000);
		loggers.log(LogStatus.INFO, "Popup window is closed");
				
		WebElement userMenu=driver.findElement(By.xpath("//div[@id='userNavButton']"));
		clickMenu(userMenu, "User menu");
		Thread.sleep(2000);
		
		WebElement userProfile=driver.findElement(By.xpath("//a[contains(text(),'My Profile')]"));
		clickMenu(userProfile, "My Profile");
		Thread.sleep(5000);
		String str="https://na132.salesforce.com/_ui/core/userprofile/UserProfilePage?tab=sfdc.ProfilePlatformFeed";
		
		checkURL(driver,str,"User Profile" );
		//if (driver.getCurrentUrl().equals("https://na132.salesforce.com/_ui/core/userprofile/UserProfilePage?tab=sfdc.ProfilePlatformFeed"))
		//	System.out.println("User Profile page is loaded successfully");
		
		WebElement editProfile=driver.findElement(By.xpath("//img[@class='recImage user']"));
		clickMenu(editProfile, "Edit Profile");
		Thread.sleep(5000);

		driver.switchTo().frame(driver.findElement(By.id("aboutMeContentId")));
		
		WebElement aboutTab=driver.findElement(By.xpath("//a[contains(text(),'About')]"));
		clickMenu(aboutTab, "About tab");
		Thread.sleep(5000);
		
		WebElement lastName=driver.findElement(By.xpath("//input[@id='lastName']"));
		String updatedLastName="Shukla";
		enterText(lastName,updatedLastName, "Last name");
		Thread.sleep(6000);
				
		WebElement saveAll=driver.findElement(By.xpath("//input[@class='zen-btn zen-primaryBtn zen-pas']"));
		clickButton(saveAll, "Save all button");
		
		driver.switchTo().parentFrame();
		
		WebElement updatedUserName=driver.findElement(By.xpath("//span[@id='tailBreadcrumbNode']"));
		System.out.println( updatedLastName);
		if (updatedUserName.getText().contains(updatedLastName))
			System.out.println(updatedUserName.getText());
		
		WebElement post=driver.findElement(By.xpath("//span[contains(@class,'publisherattachtext')][contains(text(),'Post')]"));
		clickLink(post,"Post link");
		Thread.sleep(6000);
		
		WebElement postFrame=driver.findElement(By.xpath("//iframe[contains(@title,'Rich Text Editor, publisherRichTextEditor')]"));
		driver.switchTo().frame(postFrame);
		WebElement postText=driver.findElement(By.xpath("/html[1]/body[1]"));
		String postMsg="New Post";
		enterText(postText,postMsg, "Post text box");
		driver.switchTo().parentFrame();
		Thread.sleep(6000);
		
		WebElement shareButton=driver.findElement(By.xpath("//input[@id='publishersharebutton']"));
		clickButton(shareButton, "Share button");
		
		WebElement postMsgDisplay=driver.findElement(By.xpath("//p[contains(text(),'"+ postMsg +"')]"));
		validateErrorMsg(postMsgDisplay,postMsg, "Post message");
		Thread.sleep(5000);
		
		WebElement file= driver.findElement(By.xpath("//span[contains(@class,'publisherattachtext')][contains(text(),'File')]"));
		clickLink(file, "File link");
		Thread.sleep(5000);
		
		//WebElement uploadComp=driver.findElement(By.id("chatterUploadFileAction"));
		WebElement uploadComp=driver.findElement(By.xpath("//a[@id='chatterUploadFileAction']"));
		clickButton(uploadComp, "Upload from computer button");
		Thread.sleep(5000);
		
		WebElement chooseFile=driver.findElement(By.xpath("//input[@id='chatterFile']"));
		//chooseFile.click();
		chooseFile.sendKeys("C:\\Users\\asita\\Desktop\\TekArch\\test.txt");
		 Thread.sleep(5000);
		
	
		 WebElement photoMouseHover=driver.findElement(By.id("photoSection"));
		 Actions act=new Actions(driver);
		 act.moveToElement(photoMouseHover).build().perform();
		 Thread.sleep(3000);
		 
		 WebElement addPhoto=driver.findElement(By.xpath("//a[@id='uploadLink']"));
		 addPhoto.click();
		 Thread.sleep(4000);
		 
		WebElement postFramePic=driver.findElement(By.xpath("//iframe[@id='uploadPhotoContentId']"));
		driver.switchTo().frame(postFramePic);
		
		 WebElement choosePic=driver.findElement(By.xpath("//input[@id='j_id0:uploadFileForm:uploadInputFile']"));
		 choosePic.sendKeys("C:\\Users\\asita\\Desktop\\TekArch\\image.jpg");
		 
		 Actions crop=new Actions(driver);
		 WebElement cropTracker=driver.findElement(By.xpath("//div[@class='imgCrop_selArea']//div[@class='imgCrop_clickArea']"));
		// crop.clickAndHold(choosePic).moveByOffset(30,280).release().build().perform();
		 crop.dragAndDropBy(cropTracker, 30,220).perform();
		 Thread.sleep(5000);
		 
		 WebElement savePic=driver.findElement(By.xpath("//input[@id='j_id0:j_id7:save']"));
		 savePic.click();
		 driver.switchTo().parentFrame();
		 Thread.sleep(5000);
}

}

package SalesforcePackage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ReUsableMethods {
	static ExtentTest loggers;
	static ExtentReports reports=new ExtentReports("C:\\Users\\asita\\Desktop\\TekArch\\SeleniumReport\\sample.html");
	

	public static ExtentTest createTestScriptReports(String testScriptName)
	{
		loggers=reports.startTest(testScriptName);
		return loggers;
	}
		/* Name of the Method: Click Button 
		 * 
		 * created by Rolly on 15th july 2019
		 */
	public static void clickButton(WebElement obj, String objName)
	{
		if (obj.isEnabled()) 
		{
			obj.click();
			System.out.println("Pass : Clicked "+objName);
			loggers.log(LogStatus.PASS, "Clicked "+objName);
		}
		else
		{
			System.out.println("Pass : " + objName +" is not enabled.");
			loggers.log(LogStatus.FAIL, objName +" is not enabled.");
		}
	}
	public static void clickLink(WebElement obj, String objName)
	{
		if (obj.isEnabled())
		{
			obj.click();
			System.out.println("Pass : "+objName + " is selected");
			loggers.log(LogStatus.PASS, objName + " is selected");
		}
		else
		{
			System.out.println("Pass : " + objName +" is not enabled.");
			loggers.log(LogStatus.FAIL, objName +" is not enabled.");
		}
	}

	public static void clickMenu(WebElement obj, String objName)
	{
		if (obj.isDisplayed())
		{
			obj.click();
			System.out.println("Pass : "+objName + " is selected");
			loggers.log(LogStatus.PASS, objName + " is selected");
		}
		else
		{
			System.out.println("Pass : " + objName +" is not available.");
			loggers.log(LogStatus.FAIL, objName +" is not available.");
		}
	}
	public static void checkBoxChecked(WebElement obj, String objName)
	{
		if (obj.isEnabled()) 
		{
			obj.click();
			loggers.log(LogStatus.INFO, "Clicked "+objName);
			System.out.println("Pass : Checked "+objName);
		}else
		{
			loggers.log(LogStatus.FAIL, objName +" is not enabled.");
			System.out.println("Pass : " + objName +" is not enabled.");
		}
	}
	/* Name of the Method:Enter text 
	 * 
	 * created by Rolly on 15th july 2019
	 */
	public static void enterText(WebElement obj, String text, String objName)
	{
		 if (obj.isEnabled())
		 {
			 obj.clear();
			 obj.sendKeys(text);	
			 loggers.log(LogStatus.INFO, text +" is entered in " + objName +" successfully");
			 System.out.println("Pass : " + text +" is entered in " + objName +" successfully");
		}
		 else
		 {
			loggers.log(LogStatus.FAIL, objName + " is not enabled.");
			 System.out.println("Fail :" + objName + " is not enabled.");
		 }
		}
	
	public static void clearTextBox(WebElement obj, String objName)
	{
		if (obj.isEnabled())
		{
			obj.clear();
			System.out.println("Pass : " + objName + " field is cleared.");
		    loggers.log(LogStatus.INFO, objName + " field is cleared.");
		}else
		{
			System.out.println("Fail : " + objName + " field is not enabled.");	
			loggers.log(LogStatus.FAIL, objName + " field is not enabled.");
		}
	}
	
	public static void validateErrorMsg(WebElement obj, String expectedResult, String objName) {
		if  (obj.isDisplayed())
		{
			if (obj.getText().contains(expectedResult))
			{
				 System.out.println("Pass : " + objName + " is displayed successfully.");
				loggers.log(LogStatus.PASS, objName + " is displayed successfully.");
			}
			 else
			 {
			 	System.out.println("Fail : Actual "+objName + " is not same as expected " +objName +".");
				loggers.log(LogStatus.FAIL, objName + " is not same as expected "+objName +".");
			 }
		}else
		{
			System.out.println("Fail : Error message is not displayed.");
			loggers.log(LogStatus.FAIL,"Error message is not displayed.");
		}
	}
	public static void checkURL(WebDriver driv, String urlStr, String objName)
	{
		if (driv.getCurrentUrl().equals(urlStr))
		{
			System.out.println(objName + " page is loaded successfully");
		}
	}
	public static void dropdownSelect(WebElement obj,String str, String objName) {
		if (obj.isEnabled())
		{
			Select intervalCombp=new Select(obj);
			intervalCombp.selectByVisibleText(str);
			System.out.println(objName +" is selected");
			System.out.println("Pass : " + objName + " is displayed successfully.");
			 loggers.log(LogStatus.PASS, objName + " is displayed successfully.");
		}
		else
		{
			System.out.println(objName +" is not selected");
			loggers.log(LogStatus.FAIL,objName +" is not selected");
		}
	}
}


